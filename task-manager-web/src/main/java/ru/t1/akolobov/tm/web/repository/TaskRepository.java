package ru.t1.akolobov.tm.web.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.t1.akolobov.tm.web.model.Task;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

@Repository
public final class TaskRepository {

    @NotNull
    private final Map<String, Task> tasks = new LinkedHashMap<>();

    {
        save(new Task("Alpha Task"));
        save(new Task("Betta Task"));
        save(new Task("Gamma Task"));
    }

    public void save(@NotNull final Task task) {
        tasks.put(task.getId(), task);
    }

    @NotNull
    public Collection<Task> findAll() {
        return tasks.values();
    }

    @Nullable
    public Task findById(@NotNull final String id) {
        return tasks.get(id);
    }

    public void deleteById(@NotNull final String id) {
        tasks.remove(id);
    }

}
