package ru.t1.akolobov.tm.web.configuration;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.t1.akolobov.tm.web")
public class ApplicationConfiguration {

}
