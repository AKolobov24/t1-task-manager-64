package ru.t1.akolobov.tm.exception.user;

public final class FirstNameEmptyException extends AbstractUserException {

    public FirstNameEmptyException() {
        super("Error! First name is empty...");
    }

}
