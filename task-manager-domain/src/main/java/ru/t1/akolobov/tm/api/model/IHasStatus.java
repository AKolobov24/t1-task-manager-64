package ru.t1.akolobov.tm.api.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.enumerated.Status;

public interface IHasStatus {

    @NotNull
    Status getStatus();

    void setStatus(@NotNull final Status status);

}
