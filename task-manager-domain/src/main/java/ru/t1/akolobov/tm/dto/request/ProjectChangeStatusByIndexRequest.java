package ru.t1.akolobov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectChangeStatusByIndexRequest extends AbstractUserRequest {

    @NotNull
    private Integer index;
    @Nullable
    private Status status;

    public ProjectChangeStatusByIndexRequest(@Nullable final String token) {
        super(token);
    }

}
