package ru.t1.akolobov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.akolobov.tm.dto.model.TaskDto;
import ru.t1.akolobov.tm.dto.request.TaskListRequest;
import ru.t1.akolobov.tm.dto.response.TaskListResponse;
import ru.t1.akolobov.tm.enumerated.SortBy;
import ru.t1.akolobov.tm.event.ConsoleEvent;
import ru.t1.akolobov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Component
public final class TaskListListener extends AbstractTaskListener {

    @NotNull
    public static final String NAME = "task-list";

    @NotNull
    public static final String DESCRIPTION = "Display list of all tasks.";

    @NotNull
    @Override
    public String getEventName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@taskListListener.getEventName() == #event.name")
    public void handleEvent(@NotNull final ConsoleEvent event) {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(SortBy.values()));
        @Nullable final String sortType = TerminalUtil.nextLine();
        @NotNull final TaskListRequest request = new TaskListRequest(getToken());
        request.setSortType(
                Optional.ofNullable(SortBy.toSort(sortType))
                        .orElse(SortBy.BY_CREATED)
        );
        @NotNull final TaskListResponse response = getTaskEndpoint().listTask(request);
        @NotNull final List<TaskDto> taskList = response.getTaskList();
        renderTaskList(taskList);
    }

}
