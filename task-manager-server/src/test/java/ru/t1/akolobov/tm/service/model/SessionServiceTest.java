package ru.t1.akolobov.tm.service.model;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import ru.t1.akolobov.tm.api.service.model.ISessionService;
import ru.t1.akolobov.tm.exception.field.IdEmptyException;
import ru.t1.akolobov.tm.exception.field.UserIdEmptyException;
import ru.t1.akolobov.tm.marker.UnitCategory;
import ru.t1.akolobov.tm.model.Session;
import ru.t1.akolobov.tm.repository.model.SessionRepository;
import ru.t1.akolobov.tm.repository.model.UserRepository;

import java.util.List;

import static ru.t1.akolobov.tm.data.model.TestSession.createSession;
import static ru.t1.akolobov.tm.data.model.TestSession.createSessionList;
import static ru.t1.akolobov.tm.data.model.TestUser.*;

@Setter
@Category(UnitCategory.class)
public class SessionServiceTest {

    @NotNull
    @Autowired
    private static UserRepository userRepository;

    @NotNull
    @Autowired
    private SessionRepository repository;

    @NotNull
    @Autowired
    private ISessionService service;

    @BeforeClass
    public static void addUsers() {
        userRepository.save(USER1);
        userRepository.save(USER2);
    }

    @AfterClass
    public static void clearUsers() {
        userRepository.delete(USER1);
        userRepository.delete(USER2);
    }

    @Before
    public void initRepository() {
        createSessionList(USER1).forEach(repository::save);
    }

    @After
    public void clearRepository() {
        repository.deleteByUserId(USER1_ID);
        repository.deleteByUserId(USER2_ID);
    }

    @Test
    public void add() {
        @NotNull final Session session = createSession(USER1);
        service.add(USER1_ID, session);
        Assert.assertEquals(
                session,
                repository.
                        findByUserIdAndId(session.getUser().getId(), session.getId())
                        .orElse(null)
        );
        Assert.assertThrows(UserIdEmptyException.class, () -> service.add(USER_EMPTY_ID, session));
    }

    @Test
    public void clear() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.clear(USER_EMPTY_ID));
        @NotNull final List<Session> sessionList = createSessionList(USER2);
        service.add(sessionList);
        Assert.assertFalse(service.findAll(USER2_ID).isEmpty());
        service.clear(USER2_ID);
        Assert.assertTrue(service.findAll(USER2_ID).isEmpty());
    }

    @Test
    public void existById() {
        @NotNull final Session session = createSession(USER1);
        service.add(session);
        Assert.assertTrue(service.existById(USER1_ID, session.getId()));
        Assert.assertFalse(service.existById(USER2_ID, session.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.existById(USER1_ID, USER_EMPTY_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existById(USER_EMPTY_ID, session.getId()));
    }

    @Test
    public void findAll() {
        @NotNull final List<Session> sessionList = createSessionList(USER2);
        service.add(sessionList);
        Assert.assertEquals(sessionList, service.findAll(USER2_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll(USER_EMPTY_ID));
    }

    @Test
    public void findOneById() {
        @NotNull final Session session = createSession(USER1);
        service.add(session);
        Assert.assertEquals(session, service.findOneById(USER1_ID, session.getId()).orElse(null));
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.findOneById(USER_EMPTY_ID, session.getId())
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> service.findOneById(USER1_ID, USER_EMPTY_ID)
        );
    }

    @Test
    public void getSize() {
        int size = service.findAll(USER1_ID).size();
        Assert.assertEquals(size, service.getSize(USER1_ID).intValue());
        service.add(createSession(USER1));
        Assert.assertEquals(size + 1, service.getSize(USER1_ID).intValue());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.getSize(USER_EMPTY_ID));
    }

    @Test
    public void remove() {
        @NotNull final List<Session> sessionList = service.findAll(USER1_ID);
        int size = sessionList.size();
        @NotNull final Session session = sessionList.get(size - 1);
        Assert.assertNotNull(session);
        service.remove(USER1_ID, session);
        Assert.assertFalse(service.findAll(USER1_ID).contains(session));
        Assert.assertNull(service.findOneById(USER1_ID, session.getId()).orElse(null));
        Assert.assertEquals(size - 1, service.findAll(USER1_ID).size());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.remove(USER_EMPTY_ID, session));
    }

    @Test
    public void removeById() {
        @NotNull final List<Session> sessionList = service.findAll(USER1_ID);
        int size = sessionList.size();
        @NotNull final Session session = sessionList.get(size - 1);
        Assert.assertNotNull(session);
        service.removeById(USER1_ID, session.getId());
        Assert.assertFalse(service.findAll(USER1_ID).contains(session));
        Assert.assertNull(service.findOneById(USER1_ID, session.getId()).orElse(null));
        Assert.assertEquals(size - 1, service.findAll(USER1_ID).size());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeById(USER_EMPTY_ID, session.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(USER1_ID, USER_EMPTY_ID));
    }

}
